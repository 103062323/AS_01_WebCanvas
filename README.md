# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

##Other useful widgets##

(一.)upload照片時，我做了細節的優化：
1.首先為了避免圖片上傳過大超出Canvas，我將其縮限在canvas的寬高之中。
2.但是這仍然無法避免畫面被壓縮，所以在開圖的時候，我記錄了原始的照片長寬
算出了寬/長比例，依照四種不同的失真狀態進行調整，使其在任何情況下都能在
大小有限的Canvas中開出原始比例的無失真圖片。
3.限定開啟檔案的格式，使它只會顯示圖片，不會誤開其它檔案。

(二.)刷頭形狀更改：可以按鈕改變畫筆刷頭的形狀。
(三.)Fill功能：能迅速的填滿、切換底布顏色。

## function explanation ##

`addEventListener(x,y)` 即在觸發x時會啟動y
整個canvas裡的運作主要就靠 x= mousemove、mousedown、mouseup去觸發

在此設計了var down = false去偵測現在滑鼠是按下與否。

`start()`是按下滑鼠後mousedown會行為的function，裡面根據mode的不同，會有不同的模式
x/y = e.clientX/Y - canvas.offsetLeft/Top是為了取'canvas'中準確的位置(以左上為0,0)：

mode=1時為畫畫模式，會呼叫mousedraw()進行迴圈，使畫面不斷更新
( function mousedraw(e) 會在滑鼠移動時呼叫，普通畫畫模式的stroke在其中完成[stroke()])
mode=2~4時分別為畫圓、畫長方、畫三角形模式
以上的畫圖作法大同小異，都是用beginpath()和stroke()完成，差別在呼叫的圖形函數不同
mode=5時將會把text input裡的文字以context.fillText()的方式貼上canvas

`canvas.addEventListener("mouseup")時`
將down重新改為false 並將剛才做過的動作推進URL中記錄，以方便復原[undo/redo]。

`function brushstyle(e) `
改變刷頭的形狀

`document.getElementById('file').addEventListener("change",function(e))`
用於開啟文件檔案，先var一個變數記錄所要開啟的東西，將全新的Img物件指向它，將其在canvas中
以context.drawImage()的方式開啟，同時在最後記錄此行為並且清除buff暫存的值。

`function Click()`
因為打開file的按鈕是隱藏顯示的，所以必須在js中以click的方式開啟。

`function c_color(e)`
網頁上方變換顏色的按鈕函數，呼叫時可以改變刷頭顏色、指標圖形(canvas.style.cursor)
並且由於改變的是fillStyle和strokeStyle，所以意同改變其他工具的顏色

`function clean()`
用context.clearRect()清除整個canvas版面

`function eraser(e)`
以刷頭顏色去除原先已上色的文字或圖片，進入時會將游標圖形改變

`function brushsize(e)`
改變刷頭的尺寸大小

`function fill()`
將rect填滿，可以迅速地更改畫布底色，同時亦要cPush()記錄。

`function download()`
將canvas已完成的作品以jpg的形式下載至本地端
首先先var一個變數download吃document.getElementById("download")[按鈕]
再來將目前canvas的圖案記錄進URL裡，最後在以download.setAttribute的方式將來源改寫成所記錄的畫面

`function drawCircle()`
`function drawRec()`
`function drawTri()`
三者只在裡面更改了mode的數值，真正做事的是上文提到的start()

`cPush()`
剛才文中有提到'將其記錄'的部分是以這個function實作
原理是開出一個stack去記錄前面的步驟cStep和現在的步驟

`cUndo()`
當按下Undo時會呼叫此函數，先將整個canvas重置，再從stack中拿出上一個記錄的圖檔
以new Image()的老方式把src置換成上一步，最後用onload的方式把他drawImage上canvas

`cRedo()`
作法和上述大同小異，不過在此function可以不用clearRect。

`function input()`
將mode切換成5，做事的仍然是start()

