var canvas = document.getElementById("canvaso"); 
var context = canvas.getContext("2d");
context.lineWidth = "5";

var down = false; /*預設沒按下*/ 

/*
ctx.beginPath();              
ctx.lineWidth = "5";
ctx.strokeStyle = "green";  // Green path
ctx.moveTo(0, 75);
ctx.lineTo(250, 75);
ctx.stroke();  // Draw it
*/
var mode = 1;

canvas.addEventListener("mousemove",mousedraw);
canvas.addEventListener("mousedown", start);

context.lineJoin = 'round';

function start(e)
{
    x = e.clientX - canvas.offsetLeft;
    y = e.clientY - canvas.offsetTop;
    if(mode == 1)
    {
        down = true;
        context.beginPath();
        context.moveTo(x,y); /*開始位置*/ 
        canvas.addEventListener("mousemove", mousedraw);
    }  
    else if(mode == 2)
    {  
        x = e.clientX - canvas.offsetLeft;
        y = e.clientY - canvas.offsetTop;

        context.beginPath();
        context.closePath();
        context.lineWidth = 5;
        context.arc(x, y, graphic_size*1.45+50, 0, Math.PI * 2);
        context.fillStyle = "black";
        context.stroke();

    }
    else if(mode == 3)
    {
        x = e.clientX - canvas.offsetLeft;
        y = e.clientY - canvas.offsetTop;
        
        context.beginPath();
        context.moveTo(x,y);
        context.closePath();
        context.lineWidth = 5;
        context.rect(x-graphic_size*5, y-graphic_size*10/3, graphic_size*10, graphic_size*20/3);
        context.stroke();

    }
    else if(mode == 4)
    {
        x = e.clientX - canvas.offsetLeft;
        y = e.clientY - canvas.offsetTop;
        
        context.lineWidth = 5;
        context.beginPath();
        context.moveTo(x,y);
        context.lineTo(x,y+graphic_size*10);
        context.lineTo(x+graphic_size*10,y+graphic_size*10);
        context.closePath();
        context.stroke();
    }
    else if(mode == 5)
    {
        var text = document.getElementById("text_context").value;
        //var test = "happy";
        context.fillText(text, x, y);
    }

}

canvas.addEventListener("mouseup", 
function()
{
    down =false;
    cPush();
});

function brushstyle(e)
{
    context.lineCap = e;
}

function mousedraw(e)
{
    if(mode == 1)
    {
        x = e.clientX - canvas.offsetLeft;
        y = e.clientY - canvas.offsetTop;

        if(down == true)
        {
            context.lineTo(x,y);
            context.stroke();
        }
    }

}

document.getElementById('file').addEventListener("change",
function(e)
{

    var Just = URL.createObjectURL(e.target.files[0]);
    var img = new Image();
    img.src = Just; /*賦值給image object 因為Just有URL*/ 

    img.addEventListener('load', 
    function()
    {
        imgWidth = img.naturalWidth;
        imgHeight = img.naturalHeight;
        newimgWidth = imgWidth;
        newimgHeight = imgHeight;

        var flag = 0;

        if(newimgHeight > newimgWidth && newimgHeight >460)
        {
            newimgHeight = 460;
            newimgWidth = 460 *(imgWidth/imgHeight);
        }
        else if(newimgWidth > newimgHeight && newimgWidth > 760)
        {
            newimgWidth = 760;
            newimgHeight = 760 / (imgWidth/imgHeight);
        }
        else if(newimgWidth > newimgHeight && newimgHeight >460)
        {
            newimgHeight = 460;
            newimgWidth = 460 * (imgWidth/imgHeight);
        }
        else if(newimgHeight = newimgWidth && newimgHeight > 460)
        {
            newimgHeight = 460;
            newimgWidth = 460;
        }
        else
        {
            context.drawImage(img,0,0,imgWidth,imgHeight);
            flag = 1;
        ;}

        if(!flag)context.drawImage(img,0,0,newimgWidth,newimgHeight);
        cPush();
        URL.revokeObjectURL(Just); /*清除buff裡的東西*/

    });
});

/*--------------*/ 

function Click()
{
    document.getElementById("file").click();
}

function c_color(e)
{
    mode = 1;
    context.strokeStyle = e;
    context.fillStyle = e;    
    canvas.style.cursor = "url(Brush-icon.cur),auto";
}
function clean()
{
    /*context.fillStyle = "red";
    context.fillRect(0,0,300,150);*/
    context.clearRect(0,0,760,460);
    cPush();
}
function eraser(e)
{
    mode = 1;
    context.strokeStyle = e;
    canvas.style.cursor = "url(th.cur),auto";
}

var graphic_size = 6;
function brushsize(e) {context.lineWidth = e; graphic_size = e;}

function fill(){context.fillRect(0,0,canvas.width,canvas.height); cPush();}

/*download 圖檔*/
function download(){
    var download = document.getElementById("download");
    var image = document.getElementById("canvaso").toDataURL("image/jpg")
                .replace("image/jpg", "image/octet-stream");
          download.setAttribute("href", image);
          //download.setAttribute("download","archive.png");
}
/*download 圖檔*/

/*圖形*/ 
function drawCircle() { mode = 2; canvas.style.cursor = "url(circle.cur),auto";}
function drawRec(){mode = 3; canvas.style.cursor = "url(rec.cur),auto";}
function drawTri(){mode = 4; canvas.style.cursor = "url(tri.cur),auto";}
/*圓形、長方形、三角形*/ 

/*Redo Undo*/
var cPushArray = new Array();
var cStep = -1;

function cPush()
{
    cStep++;
    if(cStep < cPushArray.length)
    {
        cPushArray.length = cStep;
    }
    cPushArray.push(canvas.toDataURL());


}

function cUndo()
{
    if(cStep>=0)
    {
        context.clearRect(0,0,760,460);
        cStep--;
        var prevPic = new Image();
        prevPic.src = cPushArray[cStep];
        prevPic.onload = function()
        {
            context.drawImage(prevPic,0,0);
        }
    

    }
}

function cRedo()
{
    if(cStep < cPushArray.length-1)
    {
        cStep++;
        var nowPic = new Image();
        nowPic.src = cPushArray[cStep];
        nowPic.onload = function()
        {
            context.clearRect(0,0,760,460);
            context.drawImage(nowPic,0,0);
        }
        //document.title = cStep + ":" + cPushArray.length;

    }
}
/*Redo Undo*/

/*font*/
function input()
{
    mode = 5;
    canvas.style.cursor ="text";
    context.font= document.getElementById('size').value + "px " + document.getElementById('font_style').value;    
}
/*font*/ 

/*font text

var font =  '14px sans-serif';
var hasInput = false;

function input(e) 
{
    if (hasInput) return;
    addInput(e.clientX, e.clientY);
}

function addInput(x, y) 
{    
    var input = document.createElement('input');
    
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 20) + 'px';
    input.style.top =(y - 6) + 'px';

    input.onkeydown = handleEnter;
    
    document.body.appendChild(input);

    input.focus(); //讓它focus在輸入框中
    
    hasInput = true;
}

function handleEnter(e) 
{
    var keyCode = e.keyCode;
    if (keyCode === 13) //如果線在按的鍵是'Enter'
    {
        drawText(this.value, e.clientX-canvas.offsetLeft, e.clientY-canvas.offsetTop); //parseInt(x,10) 將數字轉為10進位制的int
        document.body.removeChild(this);
        hasInput = false;
    }
}

function drawText(txt, x, y) 
{
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = font;
    context.fillText(txt, x , y );
}
font text*/ 